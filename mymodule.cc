#include <iostream>
#include <string>

#include <pybind11/pybind11.h>

namespace py = pybind11;

// template<typename T> std::unique_ptr<T> py_unique_ptr(T &object) {
//     py::object py_object(pybind11::cast(&object));
//     return std::unique_ptr<T>(&object, [](T* p){});
// }

class Pet {
  public:

    Pet(const std::string &name) :
    name_(name)
    {
      std::cout << "A pet named " << name_ << " came to life." << std::endl;
    }

    // Option 1 - no move constructor - moving will make a copy
    // 
    // Pet(Pet &&other)=delete;

    // Option 2 - default move constructor - members will be moved into new object
    // Pet(Pet &&other)=default;

    // Option 3 - Custom move constructor
    Pet(Pet &&other) :
      name_((std::move(other.name_)))
    {
      std::cout << "Moving " << name_ << " to new pet." << std::endl;
    }

    virtual ~Pet()
    {
      std::cout << "A pet named '" << name_ << "' died." << std::endl;
    }

    std::string GetName() {return name_;}

  private:
    std::string name_;
};

class PetHolder {
  public:
    PetHolder(std::unique_ptr<Pet> pet) :
      pet_(std::move(pet))
    {}

    std::string GetPetName() const {
        return pet_->GetName();
    }

    void SetPet(std::unique_ptr<Pet> pet) {
        pet_ = std::move(pet);
    }
  private:
    std::unique_ptr<Pet> pet_;

};

class PyPetHolder : public PetHolder {
  public:
    PyPetHolder(Pet &pet) : PetHolder(std::make_unique<Pet>(std::move(pet)))
    {}
};

PYBIND11_MODULE(mymodule, m) {
  m.doc() = R"pbdoc(
   module to demonstrate handling of std::unique_ptr arguments in pybind11.
  )pbdoc";

  py::class_<Pet>(m, "Pet")
    .def(py::init<const std::string &>());
    
  py::class_<PyPetHolder>(m, "PetHolder")
    .def(py::init<Pet&>());

}
