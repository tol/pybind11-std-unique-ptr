#!/usr/bin/env python3

# Output:

# A pet named Tweety came to life.
# Moving Tweety to new pet.
# A pet named '' died.
# A pet named 'Tweety' died.

import mymodule

t = mymodule.Pet("Tweety")

h = mymodule.PetHolder(t)

del t
del h

